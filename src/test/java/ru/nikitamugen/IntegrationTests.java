package ru.nikitamugen;

import java.util.ArrayList;

import org.apache.camel.CamelContext;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.Predicate;
import org.apache.camel.Processor;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.spring.CamelSpringDelegatingTestContextLoader;
import org.apache.camel.test.spring.CamelSpringRunner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import ru.nikitamugen.config.ApplicationConfiguration;
import ru.nikitamugen.slack.dto.Attachment;
import ru.nikitamugen.slack.dto.DeleteMessage;
import ru.nikitamugen.slack.dto.Field;
import ru.nikitamugen.slack.dto.Payload;
import ru.nikitamugen.slack.dto.PostMessage;
import ru.nikitamugen.slack.dto.UpdateMessage;
import ru.nikitamugen.slack.entity.Message;
import ru.nikitamugen.slack.repository.MessageRepository;

@RunWith(CamelSpringRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class}, loader = CamelSpringDelegatingTestContextLoader.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class IntegrationTests extends AbstractJUnit4SpringContextTests {
	
	@Autowired
    protected CamelContext camelContext;
	
	@Autowired
	private ApplicationContext applicationContext;
	
	@Autowired
	private MessageRepository messageRepository;

	private Message getFirstStoredMessage() {
		Iterable<Message> messageList = messageRepository.findAll();
		
		Message message = messageList.iterator().next();
		Assert.assertNotNull(message);
		
		return message;
	}
	
	private Processor currentMessageProcessor = new Processor() {
		
		@Override
		public void process(Exchange exchange) throws Exception {
			final String messageJsonString = exchange.getIn().getBody(String.class);
			final Payload currentMessage = Payload.fromJson(messageJsonString);
			
			exchange.getOut().setBody(currentMessage, Payload.class);
			
			if (!currentMessage.isOk())
				throw new Error(currentMessage.error.toString());
			
			messageRepository.save(currentMessage.toMessageEntity());
		}
	};
	
	@Produce(uri = "jms:queue:slack.inbox")
    protected ProducerTemplate inboxTemplate;
	
	@EndpointInject(uri = "mock:message")
    protected MockEndpoint messageEndpoint;
	
	@EndpointInject(uri = "mock:storeMessage")
    protected MockEndpoint storeMessageEndpoint;
	
	private PostMessage createPostMessage() throws EmptySettingException {
		PostMessage message = applicationContext.getBean(PostMessage.class); //new PostMessage();
		message.text = "Hello camel-slack WOOOOHAAAA!!";
		message.attachments = new ArrayList<Attachment>();
		
		Attachment attachment1 = new Attachment();
		attachment1.title = "Attachment 1 title";
		attachment1.text = "Attachment 1 text";
		attachment1.color = "#800080";
		message.attachments.add(attachment1);
		
		Field field1 = new Field();
		field1.isShort = true;
		field1.title = "Field 1 title";
		field1.value = "Field 1 value";
		
		Field field2 = new Field();
		field2.isShort = true;
		field2.title = "Field 2 title";
		field2.value = "Field 2 value";
		
		attachment1.fields = new ArrayList<Field>();
		attachment1.fields.add(field1);
		attachment1.fields.add(field2);
		
		
		Attachment attachment2 = new Attachment();
		attachment2.title = "Attachment 2 title";
		attachment2.text = "Attachment 2 text";
		attachment2.color = "#F6546A";
		message.attachments.add(attachment2);
		
		Field field3 = new Field();
		field3.isShort = false;
		field3.title = "Field 3 title";
		field3.value = "Field 3 value";
		
		attachment2.fields = new ArrayList<Field>();
		attachment2.fields.add(field3);
		
		return message;
	}
	
	private UpdateMessage createUpdateMessage() throws EmptySettingException {
		UpdateMessage message = applicationContext.getBean(UpdateMessage.class); //new UpdateMessage();
		message.text = "After update !";
		message.channel = getFirstStoredMessage().channel;
		message.ts = getFirstStoredMessage().ts;
		
		return message;
	}
	
	private DeleteMessage createDeleteMessage() throws EmptySettingException {
		DeleteMessage message = applicationContext.getBean(DeleteMessage.class); //new DeleteMessage();
		message.channel = getFirstStoredMessage().channel;
		message.ts = getFirstStoredMessage().ts;
		
		return message;
	}
	
	@Test
	public void testContext() throws Exception {
		
		// Expected context is all ready exists  
		//
		Assert.assertNotNull(camelContext);
	}
	
	@Test
	public void testSlackChatApi() throws Exception {
		testSendMessage();
		testUpdateMessage();
		testDeleteMessage();
	}
	
    public void testSendMessage() throws Exception {
		
		// Send post message
		//
		PostMessage message = createPostMessage();
		
		// Stop route to  advice with intercept
		// - to endpoint https://slack.com/api/chat.postMessage
		// - to endpoint jms:queue:store.message
		//
		camelContext.stopRoute("slack.postMessage");
		camelContext.getRouteDefinition("slack.postMessage").adviceWith(camelContext, new RouteBuilder() {
			
	        @Override
	        public void configure() throws Exception {
	        	
	            interceptSendToEndpoint("https://slack.com/api/chat.postMessage")
	            	.to("log:postMessage")
	            	.to(messageEndpoint)
	            .end();
	            		            
	            interceptSendToEndpoint("jms:queue:store.message")
            		.skipSendToOriginalEndpoint()
            		.process(currentMessageProcessor)
            		.to("log:storeMessage")
            		.to(storeMessageEndpoint)
            	.end();
	        }
	    });
		
		camelContext.startRoute("slack.postMessage");
		inboxTemplate.sendBody(message);
		
		// Expected message request string
		//
		messageEndpoint.expectedBodiesReceived(message.toRequestString());
		
		// Expected ok true
		//
		storeMessageEndpoint.expectedMessageCount(1);
		storeMessageEndpoint.expectedMessagesMatches(new Predicate() {
			
			@Override
			public boolean matches(Exchange exchange) {
				Payload storedMessage = storeMessageEndpoint.getExchanges().get(0).getIn().getBody(Payload.class);
				return (storedMessage.isOk());
			}
		});
		
		MockEndpoint.assertIsSatisfied(camelContext);
		MockEndpoint.resetMocks(camelContext);
	}
	
    public void testUpdateMessage() throws Exception {
		
		// Send update message
		//
		UpdateMessage message = createUpdateMessage();
		
		// Stop route to  advice with intercept
		// - to endpoint https://slack.com/api/chat.update
		// - to endpoint jms:queue:store.message
		//
		camelContext.stopRoute("slack.updateMessage");
		camelContext.getRouteDefinition("slack.updateMessage").adviceWith(camelContext, new RouteBuilder() {
			
	        @Override
	        public void configure() throws Exception {
	        	
	            interceptSendToEndpoint("https://slack.com/api/chat.update")
	            	.to("log:updateMessage")
	            	.to(messageEndpoint)
	            .end();
	            		            
	            interceptSendToEndpoint("jms:queue:store.message")
            		.skipSendToOriginalEndpoint()
            		.process(currentMessageProcessor)
            		.to("log:storeMessage")
            		.to(storeMessageEndpoint)
            	.end();
	        }
	    });
		
		camelContext.startRoute("slack.updateMessage");
		inboxTemplate.sendBody(message);
		
		// Expected message request string
		//
		messageEndpoint.expectedBodiesReceived(message.toRequestString());
		
		// Expected ok true
		//
		storeMessageEndpoint.expectedMessageCount(1);
		storeMessageEndpoint.expectedMessagesMatches(new Predicate() {
			
			@Override
			public boolean matches(Exchange exchange) {
				Payload storedMessage = storeMessageEndpoint.getExchanges().get(0).getIn().getBody(Payload.class);
				return (storedMessage.isOk());
			}
		});
		
		
		MockEndpoint.assertIsSatisfied(camelContext);
		MockEndpoint.resetMocks(camelContext);
	}
	
    public void testDeleteMessage() throws Exception {
		
		// Send delete message
		//
		DeleteMessage message = createDeleteMessage();
		
		// Stop route to  advice with intercept
		// - to endpoint https://slack.com/api/chat.delete
		// - to endpoint jms:queue:delete.message
		//
		camelContext.stopRoute("slack.deleteMessage");
		camelContext.getRouteDefinition("slack.deleteMessage").adviceWith(camelContext, new RouteBuilder() {
			
	        @Override
	        public void configure() throws Exception {
	        	
	            interceptSendToEndpoint("https://slack.com/api/chat.delete")
	            	.to("log:deleteMessage")
	            	.to(messageEndpoint)
	            .end();
	            		            
	            interceptSendToEndpoint("jms:queue:delete.message")
            		.skipSendToOriginalEndpoint()
            		.process(currentMessageProcessor)
            		.to("log:deleteMessage")
            		.to(storeMessageEndpoint)
            	.end();
	        }
	    });
		
		camelContext.startRoute("slack.deleteMessage");
		inboxTemplate.sendBody(message);
		
		// Expected message request string
		//
		messageEndpoint.expectedBodiesReceived(message.toRequestString());
		
		// Expected ok true
		//
		storeMessageEndpoint.expectedMessageCount(1);
		storeMessageEndpoint.expectedMessagesMatches(new Predicate() {
			
			@Override
			public boolean matches(Exchange exchange) {
				Payload storedMessage = storeMessageEndpoint.getExchanges().get(0).getIn().getBody(Payload.class);
				return (storedMessage.isOk());
			}
		});
		
		
		MockEndpoint.assertIsSatisfied(camelContext);
		MockEndpoint.resetMocks(camelContext);
	}
	
}
