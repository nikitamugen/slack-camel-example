package ru.nikitamugen.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.spring.javaconfig.CamelConfiguration;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "ru.nikitamugen.slack.route,ru.nikitamugen.jenkins.route")
public class RoutesConfiguration extends CamelConfiguration {

    @Autowired
    Logger logger;

    @Override
    protected void setupCamelContext(CamelContext camelContext) throws Exception {
        logger.info("RoutesConfiguration .setupCamelContext");

        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
        connectionFactory.setBrokerURL("vm://localhost?broker.persistent=false&broker.useJmx=false");
        connectionFactory.setTrustAllPackages(true);

        JmsComponent answer = new JmsComponent();
        answer.setConnectionFactory(connectionFactory);
        camelContext.addComponent("jms", answer);
    }
}
