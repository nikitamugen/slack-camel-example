package ru.nikitamugen.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import ru.nikitamugen.EmptySettingException;
import ru.nikitamugen.URLEncoder;
import ru.nikitamugen.service.SettingService;
import ru.nikitamugen.slack.dto.DeleteMessage;
import ru.nikitamugen.slack.dto.PostMessage;
import ru.nikitamugen.slack.dto.UpdateMessage;

@Configuration
@ComponentScan(basePackages={"ru.nikitamugen.slack.*"})
public class SlackConfiguration {

	@Autowired
	private SettingService settingService;
	
	@Bean
	public URLEncoder urlEncoder() {
		return new URLEncoder();
	}
	
	@Bean
	@Scope("prototype")
	public PostMessage postMessagePrototype() throws EmptySettingException {
		PostMessage postMessageInstance = new PostMessage();
		postMessageInstance.token = settingService.getSlackToken();
		postMessageInstance.channel = settingService.getSlackMainChannel();
	    return postMessageInstance;
	}
	
	@Bean
	@Scope("prototype")
	public UpdateMessage updateMessagePrototype() throws EmptySettingException {
		UpdateMessage updateMessageInstance = new UpdateMessage();
		updateMessageInstance.token = settingService.getSlackToken();
	    return updateMessageInstance;
	}
	
	@Bean
	@Scope("prototype")
	public DeleteMessage deleteMessagePrototype() throws EmptySettingException {
		DeleteMessage deleteMessageInstance = new DeleteMessage();
		deleteMessageInstance.token = settingService.getSlackToken();
	    return deleteMessageInstance;
	}
	
}
