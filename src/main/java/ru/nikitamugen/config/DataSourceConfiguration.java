package ru.nikitamugen.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import ru.nikitamugen.EmptySettingException;
import ru.nikitamugen.service.SettingService;

/**
 * Created by n.sokolov on 26.07.2017.
 */
@Configuration
@ComponentScan(basePackages={"ru.nikitamugen.service"})
@EnableJpaRepositories(basePackages={
		"ru.nikitamugen.slack.repository",
		"ru.nikitamugen.jenkins.repository"})
public class DataSourceConfiguration {

	@Autowired
	private SettingService settingService;

	@Bean
	public DataSource dataSource() throws EmptySettingException {
		final DriverManagerDataSource dataSource = new DriverManagerDataSource();

		dataSource.setDriverClassName(settingService.getDatabaseDriverClassName());
		dataSource.setUrl(settingService.getDatabaseConnectionUrl());
		dataSource.setUsername(settingService.getDatabaseConnectionUserName());
		dataSource.setPassword(settingService.getDatabaseConnectionPassword());

		return dataSource;
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws EmptySettingException {
		final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource());
		em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		em.setJpaProperties(settingService.getHibernateProperties());
		em.setPackagesToScan(
				"ru.nikitamugen.slack.entity",
				"ru.nikitamugen.jenkins.entity");

		return em;
	}

	@Bean
	public JpaTransactionManager transactionManager(final EntityManagerFactory entityManagerFactory) {
		final JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory);
		
		return transactionManager;
	}
}
