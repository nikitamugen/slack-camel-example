package ru.nikitamugen.config;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by n.sokolov on 26.07.2017.
 */
@Configuration
@Import({
	RoutesConfiguration.class, 
	DataSourceConfiguration.class,
	SlackConfiguration.class
	})
public class ApplicationConfiguration {
	@Bean
	Logger systemLogger() {
		return LogManager.getLogger("system");
	}
}
