package ru.nikitamugen.slack.repository;

import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;
import ru.nikitamugen.slack.entity.Message;

@Repository
public interface MessageRepository extends CrudRepository<Message, String> {
}
