package ru.nikitamugen.slack.dto;

public class ParameterPair {
	String name;
	String value;
	
	public ParameterPair(String name, String value) {
		this.name = name;
		this.value = value;
	}
	
	public String toRequestParameter() {
		return String.format("%s=%s", name, value);
	}
	
	public String toJsonParameter() {
		return String.format("\"%s\":\"%s\"", name, value);
	}
	
	public String toJsonParameterWithoutQuotes() {
		return String.format("\"%s\":%s", name, value);
	}
}
