package ru.nikitamugen.slack.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.util.StringUtils;

// https://api.slack.com/docs/message-attachments
public class Attachment implements IMessage, Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public String fallback;
	public String color;
	public String pretext;
	public String author_name;
	public String author_link;
	public String author_icon;
	public String title;
	public String title_link;
	public String text;
	public List<Field> fields;
	public String image_url;
	public String thumb_url;
	public String footer;
	public String footer_icon;
	public String ts;
	
	@Override
	public String toRequestString() {
		String requestString = "";
		
		List<ParameterPair> list = toParameterList();
		for (ParameterPair pair : list) {
			if (pair.name == "fields")
				requestString += pair.toJsonParameterWithoutQuotes() + ",";
			else
				requestString += pair.toJsonParameter() + ",";
		}
		
		if (requestString.endsWith(",")) {
			requestString = requestString.substring(0, requestString.length()-1);
		}
		
		if (!StringUtils.isEmpty(requestString)) {
			requestString = String.format("{%s}", requestString);
		}
		
		return requestString;
	}
	
	@Override
	public String toString() {
		return toRequestString();
	}

	public List<ParameterPair> toParameterList() {
		List<ParameterPair> list = new ArrayList<ParameterPair>();
		if (!StringUtils.isEmpty(fallback)) {
			list.add(new ParameterPair("fallback", fallback));
		}
		if (!StringUtils.isEmpty(color)) {
			list.add(new ParameterPair("color", color));
		}
		if (!StringUtils.isEmpty(pretext)) {
			list.add(new ParameterPair("pretext", pretext));
		}
		if (!StringUtils.isEmpty(author_name)) {
			list.add(new ParameterPair("author_name", author_name));
		}
		if (!StringUtils.isEmpty(author_link)) {
			list.add(new ParameterPair("author_link", author_link));
		}
		if (!StringUtils.isEmpty(author_icon)) {
			list.add(new ParameterPair("author_icon", author_icon));
		}
		if (!StringUtils.isEmpty(title)) {
			list.add(new ParameterPair("title", title));
		}
		if (!StringUtils.isEmpty(title_link)) {
			list.add(new ParameterPair("title_link", title_link));
		}
		if (!StringUtils.isEmpty(text)) {
			list.add(new ParameterPair("text", text));
		}
		if (fields != null) {
			list.add(new ParameterPair("fields", getFieldsString()));
		}
		if (!StringUtils.isEmpty(image_url)) {
			list.add(new ParameterPair("image_url", image_url));
		}
		if (!StringUtils.isEmpty(thumb_url)) {
			list.add(new ParameterPair("thumb_url", thumb_url));
		}
		if (!StringUtils.isEmpty(footer)) {
			list.add(new ParameterPair("footer", footer));
		}
		if (!StringUtils.isEmpty(footer_icon)) {
			list.add(new ParameterPair("footer_icon", footer_icon));
		}
		if (!StringUtils.isEmpty(ts)) {
			list.add(new ParameterPair("ts", ts));
		}
		
		return list;
	}
	
	public static Attachment fromJsonObject(JSONObject jsonObject) throws JSONException {
		Attachment attachment = new Attachment();
		
		if (jsonObject != null) {
			
			if (jsonObject.has("fallback"))
				attachment.fallback = jsonObject.getString("fallback");
			
			if (jsonObject.has("color"))
				attachment.color = jsonObject.getString("color");
			
			if (jsonObject.has("pretext"))
				attachment.pretext = jsonObject.getString("pretext");
			
			if (jsonObject.has("author_name"))
				attachment.author_name = jsonObject.getString("author_name");
			
			if (jsonObject.has("author_link"))
				attachment.author_link = jsonObject.getString("author_link");
			
			if (jsonObject.has("author_icon"))
				attachment.author_icon = jsonObject.getString("author_icon");
			
			if (jsonObject.has("title"))
				attachment.title = jsonObject.getString("title");
			
			if (jsonObject.has("title_link"))
				attachment.title_link = jsonObject.getString("title_link");
			
			if (jsonObject.has("text"))
				attachment.text = jsonObject.getString("text");
			
			if (jsonObject.has("image_url"))
				attachment.image_url = jsonObject.getString("image_url");
			
			if (jsonObject.has("image_url"))
				attachment.image_url = jsonObject.getString("image_url");
			
			if (jsonObject.has("footer"))
				attachment.footer = jsonObject.getString("footer");
			
			if (jsonObject.has("footer_icon"))
				attachment.footer_icon = jsonObject.getString("footer_icon");
			
			if (jsonObject.has("ts"))
				attachment.ts = jsonObject.getString("ts");
			
			if (jsonObject.has("fields")) {
				
				attachment.fields = new ArrayList<Field>();
				
				for (Object fieldJsonObject : jsonObject.getJSONArray("fields"))
					attachment.fields.add(Field.fromJsonObject((JSONObject)fieldJsonObject));
			}
			
		}
		
		return attachment;
	}
	
	private String getFieldsString() {
		String fieldsString = "";
		
		for (Field field : fields) {
			String fieldString = field.toRequestString();
			
			if (!StringUtils.isEmpty(fieldString)) {
				fieldsString = fieldsString+fieldString+",";
			}
		}
		
		if (fieldsString.endsWith(",")) {
			fieldsString = fieldsString.substring(0, fieldsString.length()-1);
		}
		
		return String.format("[%s]", fieldsString);
	}
	
	public ru.nikitamugen.slack.entity.Attachment toAttachmentEntity() {
		ru.nikitamugen.slack.entity.Attachment entity = new ru.nikitamugen.slack.entity.Attachment();
		entity.author_icon = this.author_icon;
		entity.author_link = this.author_link;
		entity.author_name = this.author_name;
		entity.color = this.color;
		entity.fallback = this.fallback;
		entity.footer = this.footer;
		entity.image_url = this.image_url;
		entity.pretext = this.pretext;
		entity.text = this.text;
		entity.thumb_url = this.thumb_url;
		entity.title = this.title;
		entity.title_link = this.title_link;
		entity.ts = this.ts;
		
		entity.fields = new ArrayList<ru.nikitamugen.slack.entity.Field>();
		for (Field field : this.fields) {
			entity.fields.add(field.toFieldEntity());
		}
		
		return entity;
	}
}
