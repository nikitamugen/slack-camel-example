package ru.nikitamugen.slack.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

public class UpdateMessage implements IMessage, Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public String token;
	public String ts;
	public String channel;
	public String text;
	public List<Attachment> attachments;
	public Parse parse = Parse.full;
	public Boolean link_names = true;
	public Boolean as_user = true;
	
	@Override
	public String toRequestString() {
		String requestString = "";
		
		List<ParameterPair> list = toParameterList();
		for (ParameterPair pair : list) {
			requestString += pair.toRequestParameter() + "&";
		}
		
		if (requestString.endsWith("&")) {
			requestString = requestString.substring(0, requestString.length()-1);
		}
		
		return requestString;
	}
	
	@Override
	public String toString() {
		return toRequestString();
	}
	
	public List<ParameterPair> toParameterList() {
		List<ParameterPair> list = new ArrayList<ParameterPair>();
		
		// Required
		list.add(new ParameterPair("token", token));
		list.add(new ParameterPair("ts", ts));
		list.add(new ParameterPair("channel", channel));
		list.add(new ParameterPair("text", text));
		
		// Optional
		if (attachments != null) {
			list.add(new ParameterPair("attachments", getAttachmentsString()));
		}
		if (parse != null) {
			list.add(new ParameterPair("parse", parse.toString()));
		}
		list.add(new ParameterPair("link_names", link_names.toString()));
		list.add(new ParameterPair("as_user", as_user.toString()));
		
		return list;
	}
	
	private String getAttachmentsString() {
		String attachmentsString = "";
		
		for (Attachment attachment : attachments) {
			String attachmentString = attachment.toRequestString();
			
			if (!StringUtils.isEmpty(attachmentString)) {
				attachmentsString = attachmentsString+attachmentString+",";
			}
		}
		
		if (attachmentsString.endsWith(",")) {
			attachmentsString = attachmentsString.substring(0, attachmentsString.length()-1);
		}
		
		return String.format("[%s]", attachmentsString);
	}
}
