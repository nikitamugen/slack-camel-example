package ru.nikitamugen.slack.dto;

// https://api.slack.com/docs/message-formatting
public enum Parse {
	full, none
}
