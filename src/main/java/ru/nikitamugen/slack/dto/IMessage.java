package ru.nikitamugen.slack.dto;

public interface IMessage {
	public String toRequestString();
}
