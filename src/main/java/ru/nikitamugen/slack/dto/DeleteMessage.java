package ru.nikitamugen.slack.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DeleteMessage implements IMessage, Serializable {

	private static final long serialVersionUID = 1L;
	
	public String token;
	public String ts;
	public String channel;
	public Boolean as_user = true;
	
	@Override
	public String toRequestString() {
		String requestString = "";
		
		List<ParameterPair> list = toParameterList();
		for (ParameterPair pair : list) {
			requestString += pair.toRequestParameter() + "&";
		}
		
		if (requestString.endsWith("&")) {
			requestString = requestString.substring(0, requestString.length()-1);
		}
		
		return requestString;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return toRequestString();
	}

	public List<ParameterPair> toParameterList() {
		List<ParameterPair> list = new ArrayList<ParameterPair>();
		
		// Required
		list.add(new ParameterPair("token", token));
		list.add(new ParameterPair("ts", ts));
		list.add(new ParameterPair("channel", channel));
		
		// Optional
		list.add(new ParameterPair("as_user", as_user.toString()));
		
		return list;
	}

}
