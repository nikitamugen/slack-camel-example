package ru.nikitamugen.slack.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.util.StringUtils;

// https://api.slack.com/methods/chat.postMessage
public class PostMessage implements IMessage, Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public String token;
	public String channel;
	public String text;
	public Parse parse = Parse.full;
	public Boolean link_names = true;
	public List<Attachment> attachments;
	public Boolean unfurl_links = true;
	public Boolean unfurl_media = false;
	public String username;
	public Boolean as_user = false;
	public String icon_url;
	public String icon_emoji;
	public String thread_ts;
	public Boolean reply_broadcast = true;
	
	@Override
	public String toRequestString() {
		String requestString = "";
		
		List<ParameterPair> list = toParameterList();
		for (ParameterPair pair : list) {
			requestString += pair.toRequestParameter() + "&";
		}
		
		if (requestString.endsWith("&")) {
			requestString = requestString.substring(0, requestString.length()-1);
		}
		
		return requestString;
	}
	
	@Override
	public String toString() {
		return toRequestString();
	}
	
	public List<ParameterPair> toParameterList() {
		List<ParameterPair> list = new ArrayList<ParameterPair>();
		
		// Required
		list.add(new ParameterPair("token", token));
		list.add(new ParameterPair("channel", channel));
		list.add(new ParameterPair("text", text));
		
		// Optional
		if (parse != null) {
			list.add(new ParameterPair("parse", parse.toString()));
		}
		list.add(new ParameterPair("link_names", link_names.toString()));
		if (attachments != null) {
			list.add(new ParameterPair("attachments", getAttachmentsString()));
		}
		list.add(new ParameterPair("unfurl_links", unfurl_links.toString()));
		list.add(new ParameterPair("unfurl_media", unfurl_media.toString()));
		if (!StringUtils.isEmpty(username)) {
			list.add(new ParameterPair("username", username));
		}
		list.add(new ParameterPair("as_user", as_user.toString()));
		
		if (!StringUtils.isEmpty(icon_url)) {
			list.add(new ParameterPair("icon_url", icon_url));
		}
		if (!StringUtils.isEmpty(icon_emoji)) {
			list.add(new ParameterPair("icon_emoji", icon_emoji));
		}
		if (!StringUtils.isEmpty(thread_ts)) {
			list.add(new ParameterPair("thread_ts", thread_ts));
		}
		list.add(new ParameterPair("reply_broadcast", reply_broadcast.toString()));
		
		return list;
	}
	
	private String getAttachmentsString() {
		String attachmentsString = "";
		
		for (Attachment attachment : attachments) {
			String attachmentString = attachment.toRequestString();
			
			if (!StringUtils.isEmpty(attachmentString)) {
				attachmentsString = attachmentsString+attachmentString+",";
			}
		}
		
		if (attachmentsString.endsWith(",")) {
			attachmentsString = attachmentsString.substring(0, attachmentsString.length()-1);
		}
		
		return String.format("[%s]", attachmentsString);
	}
}
