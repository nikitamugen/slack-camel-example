package ru.nikitamugen.slack.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import ru.nikitamugen.slack.entity.Message;

public class Payload implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public Boolean ok;
	public ResponseType error;
	
	// Message
	
	public String ts;
	public String channel;
	public String text;	
	public String username;
	public String icon_url;
	public String icon_emoji;
	public String thread_ts;
	
	public List<Attachment> attachments;
	
	public static Payload fromJson(String jsonString) throws JSONException {
		Payload payload = new Payload();
		
		JSONObject jsonObject = new JSONObject(jsonString);
		if (jsonObject != null) {
			
			if (jsonObject.has("ok"))
				payload.ok = jsonObject.getBoolean("ok");
			
			if (jsonObject.has("error"))
				payload.error = jsonObject.getEnum(ResponseType.class, "error");
			
			if (jsonObject.has("ts"))
				payload.ts = jsonObject.getString("ts");
			
			if (jsonObject.has("channel"))
				payload.channel = jsonObject.getString("channel");
			
			if (jsonObject.has("message")) {
				JSONObject message = jsonObject.getJSONObject("message");
				payload.text = message.getString("text");
				payload.username = message.getString("username");
				
				if (message.has("icon_url"))
					payload.icon_url = message.getString("icon_url");
				
				if (message.has("icon_emoji"))
					payload.icon_emoji = message.getString("icon_emoji");
				
				if (message.has("thread_ts"))
					payload.icon_emoji = message.getString("thread_ts");
				
				if (jsonObject.has("attachments")) {
					
					payload.attachments = new ArrayList<Attachment>();
					
					for (Object attachmentJsonObject : jsonObject.getJSONArray("attachments"))
						payload.attachments.add(Attachment.fromJsonObject((JSONObject)attachmentJsonObject));
				}
			}
		}
		
		return payload;
	}
	
	public Boolean isOk() {
		return (ok);
	}
	
	public Message toMessageEntity() {
		Message message = new Message();
		message.channel = this.channel;
		message.icon_emoji = this.icon_emoji;
		message.icon_url = this.icon_url;
		message.text = this.text;
		message.thread_ts = this.thread_ts;
		message.ts = this.ts;
		message.username = this.username;
		
		message.attachments = new ArrayList<ru.nikitamugen.slack.entity.Attachment>();
		if (this.attachments != null) {
			for (Attachment attachment : this.attachments) {
				message.attachments.add(attachment.toAttachmentEntity());
			}
		}
		
		return message;
	}
}
