package ru.nikitamugen.slack.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.util.StringUtils;

// https://api.slack.com/docs/message-attachments
public class Field implements IMessage, Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public String title;
	public String value;
	public Boolean isShort; // short - not allowed
	
	@Override
	public String toRequestString() {
		String requestString = "";
		
		List<ParameterPair> list = toParameterList();
		for (ParameterPair pair : list) {
			requestString += pair.toJsonParameter() + ",";
		}
		
		if (requestString.endsWith(",")) {
			requestString = requestString.substring(0, requestString.length()-1);
		}
		
		if (!StringUtils.isEmpty(requestString)) {
			requestString = String.format("{%s}", requestString);
		}
		
		return requestString;
	}
	
	@Override
	public String toString() {
		return toRequestString();
	}
	
	public List<ParameterPair> toParameterList() {
		List<ParameterPair> list = new ArrayList<ParameterPair>();
		
		if (!StringUtils.isEmpty(title)) {
			list.add(new ParameterPair("title", title));
		}
		if (!StringUtils.isEmpty(value)) {
			list.add(new ParameterPair("value", value));
		}
		list.add(new ParameterPair("short", isShort.toString()));
		
		return list;
	}
	
	public static Field fromJsonObject(JSONObject jsonObject) throws JSONException {
		Field field = new Field();
		
		if (jsonObject != null) {
			
			if (jsonObject.has("title"))
				field.title = jsonObject.getString("title");
			
			if (jsonObject.has("value"))
				field.value = jsonObject.getString("value");
			
			if (jsonObject.has("isShort"))
				field.isShort = jsonObject.getBoolean("isShort");
		}
		
		return field;
	}
	
	public ru.nikitamugen.slack.entity.Field toFieldEntity() {
		ru.nikitamugen.slack.entity.Field entity = new ru.nikitamugen.slack.entity.Field();
		entity.isShort = this.isShort;
		entity.title = this.title;
		entity.value = this.value;
		
		return entity;
	}
}
