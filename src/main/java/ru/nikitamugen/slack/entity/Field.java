package ru.nikitamugen.slack.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "field", catalog = "slack", uniqueConstraints = {
		@UniqueConstraint(columnNames = "ID") })
public class Field implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", length = 17,
			unique = true, nullable = false)
	public Long id;

	@Column(name = "TITLE", length = 512)
	public String title;

	@Column(name = "VALUE", length = 3000)
	public String value;

	@Column(name = "IS_SHORT")
	public Boolean isShort; // short - not allowed

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ATTACHMENT_ID", nullable = false)
	public Attachment attachment;
}
