package ru.nikitamugen.slack.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "message", schema="slack", uniqueConstraints = {
	   @UniqueConstraint(columnNames = "TS") })
public class Message implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "TS", length = 17,
			unique = true, nullable = false)
	public String ts;
	
	@Column(name = "CHANNEL", length = 36,
			nullable = false)
	public String channel;
	
	@Column(name = "TEXT", length = 3000)
	public String text;
	
	@Column(name = "USER_NAME", length = 100)
	public String username;
	
	@Column(name = "ICON_URL", length = 500)
	public String icon_url;
	
	@Column(name = "ICON_EMOJI", length = 100)
	public String icon_emoji;
	
	@Column(name = "THREAD_TS", length = 17)
	public String thread_ts;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "message")
	public List<Attachment> attachments;
}
