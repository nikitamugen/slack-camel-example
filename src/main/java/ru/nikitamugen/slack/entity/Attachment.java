package ru.nikitamugen.slack.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "attachment", catalog = "slack", uniqueConstraints = {
		@UniqueConstraint(columnNames = "TS") })
public class Attachment implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "TS", length = 17,
			unique = true, nullable = false)
	public String ts;

	@Column(name = "FALLBACK", length = 50)
	public String fallback;

	@Column(name = "COLOR", length = 50)
	public String color;

	@Column(name = "PRETEXT", length = 256)
	public String pretext;

	@Column(name = "AUTHOR_NAME", length = 256)
	public String author_name;

	@Column(name = "AUTHOR_LINK", length = 256)
	public String author_link;

	@Column(name = "AUTHOR_ICON", length = 256)
	public String author_icon;

	@Column(name = "TITLE", length = 512)
	public String title;

	@Column(name = "TITLE_LINK", length = 256)
	public String title_link;

	@Column(name = "TEXT", length = 3000,
			nullable = false)
	public String text;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "attachment")
	public List<Field> fields;

	@Column(name = "IMAGE_URL", length = 256)
	public String image_url;

	@Column(name = "THUMB_URL", length = 256)
	public String thumb_url;

	@Column(name = "FOOTER", length = 512)
	public String footer;

	@Column(name = "FOOTER_ICON", length = 256)
	public String footer_icon;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MESSAGE_ID", nullable = false)
	public Message message;
	
}
