package ru.nikitamugen.slack.route;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.http.entity.ContentType;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ru.nikitamugen.service.SettingService;
import ru.nikitamugen.slack.dto.DeleteMessage;
import ru.nikitamugen.slack.dto.PostMessage;
import ru.nikitamugen.slack.dto.UpdateMessage;

@Component
public class SlackRoutes extends RouteBuilder {

    @Autowired
    private PayloadProcessor messageProcessor;

    @Autowired
    Logger logger;

    @Autowired
    SettingService settingService;
    
    @Override
    public void configure() throws Exception {

        ///////////////////////////////
        // Error handlers
        ///////////////////////////////

        from("direct:catch")
        .process(new Processor() {

            @Override
            public void process(Exchange exchange) throws Exception {
                Throwable caused = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Throwable.class);
                logger.error(" --- faults | caused:" + caused.toString());
            }
        });

        ///////////////////////////////
        // Slack Message Routing
        ///////////////////////////////

        from("jms:queue:slack.inbox")
        .to("direct:slack.send");

        from("direct:slack.send")
        .choice()
            .when(body().isInstanceOf(PostMessage.class))
                .convertBodyTo(String.class)
                .to("direct:slack.postMessage")
            .endChoice()
            .when(body().isInstanceOf(UpdateMessage.class))
                .convertBodyTo(String.class)
                .to("direct:slack.updateMessage")
            .endChoice()
            .when(body().isInstanceOf(DeleteMessage.class))
                .convertBodyTo(String.class)
                .to("direct:slack.deleteMessage")
            .endChoice()
        .end();

        ///////////////////////////////
        // Slack Post Message
        ///////////////////////////////

        from("direct:slack.postMessage")
        .id("slack.postMessage")
        .log("=================== postMessage started ===================")
        .setHeader(Exchange.CONTENT_TYPE, constant(ContentType.DEFAULT_TEXT))
        .setHeader(Exchange.HTTP_METHOD, constant("POST"))
        .setHeader(Exchange.HTTP_QUERY, simple("${body}"))
        .doTry()
            .to("https://slack.com/api/chat.postMessage")
            .to("direct:slack.store.message")
        .doCatch(Exception.class)
            .to("direct:catch")
        .end()
        .log("=================== postMessage ended ===================");

        ///////////////////////////////
        // Slack Update Message
        ///////////////////////////////

        from("direct:slack.updateMessage")
        .id("slack.updateMessage")
        .log("=================== updateMessage started ===================")
        .setHeader(Exchange.CONTENT_TYPE, constant(ContentType.DEFAULT_TEXT))
        .setHeader(Exchange.HTTP_METHOD, constant("POST"))
        .setHeader(Exchange.HTTP_QUERY, simple("${body}"))
        .doTry()
            .to("https://slack.com/api/chat.update")
            .to("direct:slack.store.message")
        .doCatch(Exception.class)
            .to("direct:catch")
        .end()
        .log("=================== updateMessage ended ===================");
        
        ///////////////////////////////
        // Slack Delete Message
        ///////////////////////////////

        from("direct:slack.deleteMessage")
        .id("slack.deleteMessage")
        .log("=================== delete started ===================")
        .setHeader(Exchange.CONTENT_TYPE, constant(ContentType.DEFAULT_TEXT))
        .setHeader(Exchange.HTTP_METHOD, constant("POST"))
        .setHeader(Exchange.HTTP_QUERY, simple("${body}"))
        .doTry()
            .to("https://slack.com/api/chat.delete")
            .to("direct:slack.delete.message")
        .doCatch(Exception.class)
            .to("direct:catch")
        .end()
        .log("=================== delete ended ===================");

        ///////////////////////////////
        // Store Message
        ///////////////////////////////

        from("direct:slack.store.message")
        .id("store.message")
        .log("=================== store.message started ===================")
        .process(messageProcessor)
        .log("=================== store.message ended ===================");

        ///////////////////////////////
        // Delete Message
        ///////////////////////////////

        from("direct:slack.delete.message")
        .id("delete.message")
        .log("=================== delete.message started ===================")
        .process(messageProcessor)
        .log("=================== delete.message ended ===================");
    }
}
