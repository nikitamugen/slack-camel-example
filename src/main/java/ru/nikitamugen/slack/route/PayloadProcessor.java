package ru.nikitamugen.slack.route;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.nikitamugen.slack.dto.Payload;
import ru.nikitamugen.slack.entity.Message;
import ru.nikitamugen.slack.repository.MessageRepository;

@Component
public class PayloadProcessor implements Processor {

    @Autowired
    MessageRepository messageRepository;

    @Override
    public void process(Exchange exchange) throws Exception {
        String messageJsonString = exchange.getIn().getBody(String.class);
        Payload payload = Payload.fromJson(messageJsonString);

        exchange.getOut().setBody(payload, Payload.class);

        if (!payload.isOk())
            throw new Error(payload.error.toString());

        Message message = messageRepository.save(payload.toMessageEntity());
        exchange.getOut().setBody(message);
        exchange.getOut().setHeader("currentChannel", message.channel);
    }
}
