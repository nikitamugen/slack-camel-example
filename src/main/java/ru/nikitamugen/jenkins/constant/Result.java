package ru.nikitamugen.jenkins.constant;

public class Result {
	public static final String RESULT_ABORTED = "ABORTED";
	public static final String RESULT_FAILURE = "FAILURE";
	public static final String RESULT_NOT_BUILT = "NOT_BUILT";
	public static final String RESULT_SUCCESS = "SUCCESS";
	public static final String RESULT_UNSTABLE = "UNSTABLE";

	public static final String RESULT_ABORTED_COLOR = "#262228";
	public static final String RESULT_FAILURE_COLOR = "#af473c";
	public static final String RESULT_NOT_BUILT_COLOR = "#1e90ff";
	public static final String RESULT_SUCCESS_COLOR = "#007849";
	public static final String RESULT_UNSTABLE_COLOR = "#ffce00";
	
	public static final String getColorByResult(String result) {
		String color;
		
		switch(result) {
		case RESULT_ABORTED: 
			color = RESULT_ABORTED_COLOR;
			break;
			
		case RESULT_FAILURE: 
			color = RESULT_FAILURE_COLOR;
			break;
		
		case RESULT_SUCCESS: 
			color = RESULT_SUCCESS_COLOR;
			break;
		
		case RESULT_UNSTABLE: 
			color = RESULT_UNSTABLE_COLOR;
			break;
			
		case RESULT_NOT_BUILT: 
		default: 
			color = RESULT_NOT_BUILT_COLOR;
		}
		
		return color;
	}
	
	static final String RESULT_ABORTED_TEXT = "Отменено";
	static final String RESULT_FAILURE_TEXT = "Провал!";
	static final String RESULT_NOT_BUILT_TEXT = "В процессе";
	static final String RESULT_SUCCESS_TEXT = "Успех!";
	static final String RESULT_UNSTABLE_TEXT = "Не стабильна";
	
	public static final String getTextByResult(String result) {
		String text;
		
		switch(result) {
		case RESULT_ABORTED: 
			text = RESULT_ABORTED_TEXT;
			break;
			
		case RESULT_FAILURE: 
			text = RESULT_FAILURE_TEXT;
			break;
		
		case RESULT_SUCCESS: 
			text = RESULT_SUCCESS_TEXT;
			break;
		
		case RESULT_UNSTABLE: 
			text = RESULT_UNSTABLE_TEXT;
			break;
			
		case RESULT_NOT_BUILT: 
		default: 
			text = RESULT_NOT_BUILT_TEXT;
		}
		
		return text;
	}
	
}