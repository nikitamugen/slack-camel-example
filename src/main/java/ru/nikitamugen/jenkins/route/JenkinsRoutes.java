package ru.nikitamugen.jenkins.route;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import ru.nikitamugen.EmptySettingException;
import ru.nikitamugen.URLEncoder;
import ru.nikitamugen.jenkins.constant.Result;
import ru.nikitamugen.jenkins.entity.Build;
import ru.nikitamugen.jenkins.entity.Parameter;
import ru.nikitamugen.jenkins.repository.BuildRepository;
import ru.nikitamugen.jenkins.repository.ParameterRepository;
import ru.nikitamugen.service.SettingService;
import ru.nikitamugen.slack.dto.*;
import ru.nikitamugen.slack.entity.Message;
import ru.nikitamugen.slack.repository.MessageRepository;

import java.util.ArrayList;
import java.util.List;

@Component
public class JenkinsRoutes extends RouteBuilder {

    @Autowired
    Logger logger;

    @Autowired
    SettingService settingService;

    @Autowired
    URLEncoder encoder;

    @Autowired
    BuildRepository buildRepository;

    @Autowired
    MessageRepository messageRepository;

    @Autowired
    ParameterRepository parameterRepository;

    private static Boolean initialized = false;

    private final String getJenkinsBasicAuthString() {
        String authString = settingService.getJenkinsUserName() + ":" + settingService.getJenkinsUserToken();
        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        String authStringEnc = new String(authEncBytes);

        return "Basic " + authStringEnc;
    }

    private final String getBuildMessageText(Build build) {
        return encoder.encodeBody("Сборка задачи '" + build.jobName + "' номер '" + build.number + "'");
    }

    private final List<Field> getBuildParameterFields(Build build) {

        List<Field> fieldList = new ArrayList<Field>();
        List<Parameter> parameterList = parameterRepository.findAllByBuild(build);

        for (int parameterIndex = 0; parameterIndex < parameterList.size(); parameterIndex++) {

            Parameter parameter = parameterList.get(parameterIndex);

            Field parameterField = new Field();
            parameterField.title = parameter.name;
            parameterField.value = encoder.encodeBody(
                    StringUtils.replace(parameter.value, "\"", ""));
            parameterField.isShort = false;

            fieldList.add(parameterField);
        }

        return fieldList;
    }

    private final String formatJenkinsDuration(Long duration) {
        long absSeconds = Math.abs(duration / 1000);
        String positive = String.format(
                "%d:%02d:%02d",
                absSeconds / 3600,
                (absSeconds % 3600) / 60,
                absSeconds % 60);
        return duration < 0 ? "-" + positive : positive;
    }

    private final IMessage createMessageByBuild(Build build) throws EmptySettingException {
        IMessage result;

        Field resultField = new Field();
        resultField.isShort = true;
        resultField.title = encoder.encodeBody("Статус");
        resultField.value = encoder.encodeBody(Result.getTextByResult(build.result));

        Attachment attachment = new Attachment();
        attachment.color = Result.getColorByResult(build.result);
        attachment.fields = getBuildParameterFields(build);
        attachment.fields.add(resultField);
        attachment.title = getBuildMessageText(build);
        attachment.title_link = build.url;

        if (StringUtils.isEmpty(build.messageTS)) {
            PostMessage postMessage = new PostMessage();
            postMessage.text = "";
            postMessage.attachments = new ArrayList<Attachment>();
            postMessage.attachments.add(attachment);
            postMessage.channel = settingService.getSlackMainChannel();
            postMessage.token = settingService.getSlackToken();

            result = postMessage;
        } else {
            Message message = messageRepository.findOne(build.messageTS);

            UpdateMessage updateMessage = new UpdateMessage();
            updateMessage.text = "";
            updateMessage.attachments = new ArrayList<Attachment>();
            updateMessage.attachments.add(attachment);
            updateMessage.ts = message.ts;
            updateMessage.channel = message.channel;
            updateMessage.token = settingService.getSlackToken();

            result = updateMessage;
        }

        if ((build.result == Result.RESULT_NOT_BUILT) || StringUtils.isEmpty(build.result)) {
            Field estimatedDurationField = new Field();
            estimatedDurationField.isShort = true;
            estimatedDurationField.title = encoder.encodeBody("Предполагаемая продолжительность");
            estimatedDurationField.value = formatJenkinsDuration(build.estimatedDuration);
            attachment.fields.add(estimatedDurationField);
        } else {
            Field durationField = new Field();
            durationField.isShort = true;
            durationField.title = encoder.encodeBody("Продолжительность");
            durationField.value = formatJenkinsDuration(build.duration);
            attachment.fields.add(durationField);
        }

        return result;
    }

    @Override
    public void configure() throws Exception {
        logger.info("JenkinsRoutes .configure");

        ///////////////////////////////
        // Jenkins Routing
        ///////////////////////////////

        from("timer://buildGetTimer?period=10s")
                //.log("[TIMER] - - - buildGetTimer")
                .to("direct:jenkins.getJobUrls");

        ///////////////////////////////
        // Get jenkins jobs
        ///////////////////////////////

        from("jms:queue:jenkins.getJobUrls")
                .to("direct:jenkins.getJobUrls");

        from("direct:jenkins.getJobUrls")
                .setHeader("jenkinsHost", constant(settingService.getJenkinsHost()))
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .setHeader("Authorization", constant(getJenkinsBasicAuthString()))
                .toD("http://${header.jenkinsHost}/api/xml?tree=jobs[name]")
                .split(xpath("/hudson/job"))
                .setHeader("jobName", xpath("/job/name/text()"))
                .filter().method(SettingService.class, "isJenkinsJobNameAllowed")
                .to("direct:jenkins.getBuildsByJob")
                .end()
                .end()
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        initialized = true;
                    }
                });

        ///////////////////////////////
        // Get jenkins builds by job
        ///////////////////////////////

        from("jms:queue:jenkins.getBuildsByJob")
                .to("direct:jenkins.getBuildsByJob");

        from("direct:jenkins.getBuildsByJob")
                .setHeader("jenkinsHost", constant(settingService.getJenkinsHost()))
                .setHeader("jobName", xpath("/job/name/text()"))
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .setHeader("Authorization", constant(getJenkinsBasicAuthString()))                //.log("[INFO] - - - get builds for ${header.jenkinsHost} and job ${header.jobName}")
                .toD("http://${header.jenkinsHost}/job/${header.jobName}/api/xml?tree=builds[number,result,duration,estimatedDuration,url,actions[parameters[name,value]]]{0,5}")
                .split(xpath("//build"))
                .to("direct:processJenkinsBuild")
                .choice()
                .when(body().isInstanceOf(IMessage.class))
                .to("direct:slack.send")
                .to("direct:processJenkinsBuildMessage")
                .endChoice()
                .end();

        ///////////////////////////////
        // Process jenkins build
        ///////////////////////////////

        from("direct:processJenkinsBuild")
                .process(new Processor() {

                    @Override
                    public void process(Exchange exchange) throws Exception {
                        String jobName = exchange.getIn().getHeader("jobName", String.class);
                        Long number = xpath("//number/text()").evaluate(exchange, Long.class);

                        Boolean needSaveParameters = false;
                        Build build = buildRepository.findFirstByJobNameAndNumber(jobName, number);
                        if (build == null) {
                            build = new Build();
                            build.jobName = jobName;
                            build.number = number;

                            needSaveParameters = true;
                        }

                        String result = xpath("//result/text()").evaluate(exchange, String.class);
                        Boolean needNotify = ((!result.equals(build.result)) || StringUtils.isEmpty(result));
                        build.result = result;
                        build.duration = xpath("//duration/text()").evaluate(exchange, Long.class);
                        build.estimatedDuration = xpath("//estimatedDuration/text()").evaluate(exchange, Long.class);
                        build.url = xpath("//url/text()").evaluate(exchange, String.class);

                        build = buildRepository.save(build);

                        if (needSaveParameters) {
                            NodeList parameterNodeList = xpath("//parameter").evaluate(exchange, NodeList.class);
                            for (int nodeIndex = 0; nodeIndex < parameterNodeList.getLength(); nodeIndex++) {

                                Node parameterNode = parameterNodeList.item(nodeIndex);
                                String parameterName = parameterNode.getFirstChild().getTextContent();
                                String parameterValue = parameterNode.getLastChild().getTextContent();

                                Parameter parameter = new Parameter();
                                parameter.name = parameterName;
                                parameter.value = parameterValue;
                                parameter.build = build;

                                parameterRepository.save(parameter);
                            }
                        }

                        if (needNotify && initialized) {
                            IMessage message = createMessageByBuild(build);
                            exchange.getOut().setBody(message);
                            exchange.setProperty("buildId", build.id);
                        }
                    }
                });

        from("direct:processJenkinsBuildMessage")
                .process(new Processor() {

                    @Override
                    public void process(Exchange exchange) throws Exception {
                        Long buildId = exchange.getProperty("buildId", Long.class);
                        Build build = buildRepository.findOne(buildId);

                        Message message = exchange.getIn().getBody(Message.class);
                        build.messageTS = message.ts;

                        build = buildRepository.save(build);

                        exchange.getOut().setHeader("currentChannel", message.channel);
                        exchange.getOut().setHeader("currentTS", message.ts);
                    }
                });

    }
}
