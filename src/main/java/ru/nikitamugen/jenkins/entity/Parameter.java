package ru.nikitamugen.jenkins.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "parameter", catalog = "jenkins", uniqueConstraints = {
        @UniqueConstraint(columnNames = "ID")})
public class Parameter implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", length = 17,
            unique = true, nullable = false)
    public Long id;

    @Column(name = "NAME", length = 512)
    public String name;

    @Column(name = "VALUE", length = 3000)
    public String value;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BUILD_ID", nullable = false)
    public Build build;
}
