package ru.nikitamugen.jenkins.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by n.sokolov on 03.08.2017.
 */
@Entity
@Table(name = "build", schema = "jenkins")
public class Build implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    public Long id;

    @Column(name = "JOB_NAME", nullable = false)
    public String jobName;

    @Column(name = "NUMBER", nullable = false)
    public Long number;

    @Column(name = "RESULT", length = 200)
    public String result;

    @Column(name = "ESTIMATED_DURATION")
    public long estimatedDuration;

    @Column(name = "DURATION")
    public long duration;

    @Column(name = "MESSAGE_TS", length = 17)
    public String messageTS;

    @Column(name = "URL", length = 1000)
    public String url;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "build")
    public List<Parameter> parameters;
}
