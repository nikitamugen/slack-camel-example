package ru.nikitamugen.jenkins.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.nikitamugen.jenkins.entity.Build;
import ru.nikitamugen.jenkins.entity.Parameter;

import java.util.List;

@Repository
public interface ParameterRepository extends CrudRepository<Parameter, Long> {

	List<Parameter> findAllByBuild(Build build);
}
