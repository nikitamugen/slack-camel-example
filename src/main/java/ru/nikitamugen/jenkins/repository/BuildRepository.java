package ru.nikitamugen.jenkins.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ru.nikitamugen.jenkins.entity.Build;

@Repository
public interface BuildRepository extends CrudRepository<Build, Long> {

	Build findFirstByJobNameAndNumber(String jobName, Long number);
}
