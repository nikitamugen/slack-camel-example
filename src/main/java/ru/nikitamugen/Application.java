package ru.nikitamugen;

import org.apache.camel.spring.javaconfig.Main;

import ru.nikitamugen.config.ApplicationConfiguration;

public class Application {

	private static Main main;
	
	public static void main(String[] args) {
		try {
			System.out.println( "STARTING slack.camel" );
			
			main = createMainInstance();
			main.run();
			
			System.out.println( "STARTING slack.camel complete ");
        }
        catch (Exception ex) {
	        System.out.println( "STARTING slack.camel fault: "+ex.getLocalizedMessage() );
        }
	}
	
	private static Main createMainInstance() {
		Main main = new Main();
		main.setConfigClass(ApplicationConfiguration.class);
		return main;
	}

}
