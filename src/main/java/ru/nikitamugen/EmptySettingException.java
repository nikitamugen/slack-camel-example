package ru.nikitamugen;

public class EmptySettingException extends Exception {
	
	private static final long serialVersionUID = -3726730506803268257L;
	private static final String message = "[EMPTY] ";
	
	public EmptySettingException(String prop) {
		super(message + prop);
	}
	
	
}
