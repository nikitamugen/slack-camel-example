package ru.nikitamugen;

import java.io.UnsupportedEncodingException;

import org.springframework.stereotype.Component;

@Component
public class URLEncoder {
	
	private static final String codepage = "UTF-8";
	
	public String encodeBody(String body) {
		String encodedBody = "";
		try {
			encodedBody = java.net.URLEncoder.encode(body, codepage);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return encodedBody;
	}
}
