package ru.nikitamugen.service;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.nikitamugen.EmptySettingException;

import java.util.Properties;

@Service
@PropertySources({
        @PropertySource(value = "datasource.properties"),
        @PropertySource(value = "file:${properties.path}/slack.properties"),
        @PropertySource(value = "file:${properties.path}/jenkins.properties")
})
public class SettingService {

    private static String defaultPropValue = "";
    private static int defaultConnectionPoolSize = 1;

    @Autowired
    private Environment env;

    @Autowired
    Logger logger;

    public String getDatabaseDriverClassName() throws EmptySettingException {
        final String propName = "database.connection.driver_class";
        String prop = env.getProperty(propName);
        if (StringUtils.isEmpty(prop)) {
            throw new EmptySettingException(propName);
        }

        return prop;
    }

    public String getDatabaseConnectionUrl() throws EmptySettingException {
        final String propName = "database.connection.url";
        String prop = env.getProperty(propName);
        if (StringUtils.isEmpty(prop)) {
            throw new EmptySettingException(propName);
        }

        return prop;
    }

    public String getDatabaseConnectionUserName() {
        final String propName = "database.connection.username";
        String prop = env.getProperty(propName, defaultPropValue);

        return prop;
    }

    public String getDatabaseConnectionPassword() {
        String propName = "database.connection.password";
        String prop = env.getProperty(propName, defaultPropValue);

        return prop;
    }

    public String getDatabaseConnectionDefaultSchema() {
        final String propName = "database.connection.default_schema";
        String prop = env.getProperty(propName, defaultPropValue);

        return prop;
    }

    public int getJdbcConnectionPoolSize() {
        final String propName = "jdbc.connection.pool_size";
        int prop = env.getProperty(propName, int.class, defaultConnectionPoolSize);

        return prop;
    }

    public String getHibernateDialect() throws EmptySettingException {
        final String propName = "hibernate.dialect";
        String prop = env.getProperty(propName);
        if (StringUtils.isEmpty(prop)) {
            throw new EmptySettingException(propName);
        }

        return prop;
    }

    public String getHibernateShowSql() throws EmptySettingException {
        final String propName = "hibernate.show_sql";
        String prop = env.getProperty(propName);
        if (StringUtils.isEmpty(prop)) {
            throw new EmptySettingException(propName);
        }

        return prop;
    }

    public String getHibernateHBM2DDLAuto() throws EmptySettingException {
        final String propName = "hibernate.hbm2ddl.auto";
        String prop = env.getProperty(propName);
        if (StringUtils.isEmpty(prop)) {
            throw new EmptySettingException(propName);
        }

        return prop;
    }

    public String getHibernateCacheProviderClass() throws EmptySettingException {
        final String propName = "hibernate.cache.provider_class";
        String prop = env.getProperty(propName);
        if (StringUtils.isEmpty(prop)) {
            throw new EmptySettingException(propName);
        }

        return prop;
    }

    public Properties getHibernateProperties() throws EmptySettingException {
        final Properties hibernateProperties = new Properties();

        hibernateProperties.setProperty("hibernate.hbm2ddl.auto", getHibernateHBM2DDLAuto());
        hibernateProperties.setProperty("hibernate.dialect", getHibernateDialect());
        hibernateProperties.setProperty("hibernate.show_sql", getHibernateShowSql());

        return hibernateProperties;
    }

    public String getSlackToken() throws EmptySettingException {
        final String propName = "slack.token";
        String prop = env.getProperty(propName);
        if (StringUtils.isEmpty(prop)) {
            throw new EmptySettingException(propName);
        }

        return prop;
    }

    public String getSlackMainChannel() throws EmptySettingException {
        final String propName = "slack.mainChannel";
        String prop = env.getProperty(propName);
        if (StringUtils.isEmpty(prop)) {
            throw new EmptySettingException(propName);
        }

        return prop;
    }

    public String getJenkinsHost() throws EmptySettingException {
        final String propName = "jenkins.host";
        String prop = env.getProperty(propName);
        if (StringUtils.isEmpty(prop)) {
            throw new EmptySettingException(propName);
        }

        return prop;
    }

    public String getJenkinsUserName() {
        final String propName = "jenkins.user_name";
        return env.getProperty(propName);
    }

    public String getJenkinsUserToken() {
        final String propName = "jenkins.user_token";
        return env.getProperty(propName);
    }

    public String getJenkinsAllowedJobNames() {
        final String propName = "jenkins.allowed_job_names";
        return env.getProperty(propName, "");
    }

    public Boolean isJenkinsAllowedJobNamesEmpty() {
        return StringUtils.isEmpty(getJenkinsAllowedJobNames());
    }

    public String getJenkinsProhibitedJobNames() {
        final String propName = "jenkins.prohibited_job_names";
        return env.getProperty(propName, "");
    }

    public Boolean isJenkinsProhibitedJobNamesEmpty() {
        return StringUtils.isEmpty(getJenkinsProhibitedJobNames());
    }

    private Boolean isJenkinsJobNameInAllowedJobs(String jobName) {
        String[] allowedJobNames = getJenkinsAllowedJobNames().split(",");

        for (String allowedJobName : allowedJobNames) {

            if (allowedJobName.equals(jobName)) {
                return true;
            }
        }
        return false;
    }

    private Boolean isJenkinsJobNameInProhibitedJobs(String jobName) {
        String[] prohibitedJobNames = getJenkinsProhibitedJobNames().split(",");

        for (String prohibitedJobName : prohibitedJobNames) {

            if (prohibitedJobName.equals(jobName)) {
                return true;
            }
        }
        return false;
    }

    public Boolean isJenkinsJobNameAllowed(@Header("jobName") String jobName) {

        return (isJenkinsAllowedJobNamesEmpty() || isJenkinsJobNameInAllowedJobs(jobName)) &&
                (isJenkinsProhibitedJobNamesEmpty() || !(isJenkinsJobNameInProhibitedJobs(jobName)));
    }

}
